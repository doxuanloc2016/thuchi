package com.example.locku.assignment_mob201;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;

public class MoneyAdapter extends BaseAdapter {
    Context context;
    ArrayList<Money> ds;
    MoneyAdapter(Context context, ArrayList<Money> ds){
        this.context = context;
        this.ds = ds;
    }
    @Override
    public int getCount(){
        return ds.size();
    }
    @Override
    public Objects getItem(int position){
        return  null;
    }
    @Override
    public long getItemId(int position){
        return  0;
    }

    public String doisosangtien(int x)
    {
        String chuoi = String.valueOf(x);
        double d=Double.parseDouble(chuoi);
        DecimalFormat f=new DecimalFormat("#,###"+" "+"đ");
        return f.format(d);
    }

    @Override
    public View getView(int position, View contertView, ViewGroup parent){
        View view;
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        view = inflater.inflate(R.layout.money, null);
        TextView showMoney = (TextView) view.findViewById(R.id.textMoney);
        TextView showNgay = (TextView) view.findViewById(R.id.textNgayThang);
        TextView showGhiChu = (TextView) view.findViewById(R.id.textGhichu);
        Money money = ds.get(position);
        showMoney.setText(doisosangtien(money.tien));
        showNgay.setText(money.NgayThang);
        showGhiChu.setText(money.ghichu);
        return view;
    }
}
