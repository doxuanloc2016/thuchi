package com.example.locku.assignment_mob201;


import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DangNhapFragment extends Fragment implements View.OnClickListener{
    private Button btnlogin;
    private EditText etEmail, etPass;
    private DbHelper db;
    private Session session;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        final View view = inflater.inflate(R.layout.tabdangnhap, container, false);

        db = new DbHelper(getActivity());
        session = new Session(getActivity());
        btnlogin = (Button)view.findViewById(R.id.btn_login);
        etEmail = (EditText)view.findViewById(R.id.et_email);
        etPass = (EditText)view.findViewById(R.id.et_password);
        btnlogin.setOnClickListener(this);

        if(session.loggedin()){
            startActivity(new Intent(getActivity(),MainActivity.class));
            getActivity().finish();
        }
        return view;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_login:
                login();
                break;
        }
    }

    private void login(){
        String email = etEmail.getText().toString();
        String pass = etPass.getText().toString();
        if(db.getUser(email,pass)){
            session.setLoggedin(true);
            if(email.equals("admin@gmail.com") && pass.equals("admin")){
                startActivity(new Intent(getActivity(), UserManagement.class));
            } else {
                startActivity(new Intent(getActivity(), MainActivity.class));
            }
            getActivity().finish();
        }
        else if (!validateEmail(etEmail.getText().toString())){
            etEmail.setError("Invalid Email");
            etEmail.requestFocus();
        }
        else if (!validateEmail(etPass.getText().toString())){
            etPass.setError("Invalid Password");
            etPass.requestFocus();
        }
    }

    private boolean validateEmail(String email) {
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(email);
        return  matcher.matches();
    }
}
