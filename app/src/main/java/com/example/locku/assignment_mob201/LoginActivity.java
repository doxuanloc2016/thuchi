package com.example.locku.assignment_mob201;


import android.content.res.Resources;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TabHost;

public class LoginActivity extends AppCompatActivity {
    FragmentTabHost tabHost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        inicializarTabs();
    }

    private void inicializarTabs() {
        tabHost = (FragmentTabHost) findViewById(R.id.tabHost);
        tabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);

        Resources res = getResources();
        TabHost.TabSpec tabdangnhap = tabHost.newTabSpec("tabdangnhap");
        TabHost.TabSpec tabdangki = tabHost.newTabSpec("tabdangki");

        tabdangnhap.setIndicator(res.getString(R.string.tabdangnhap), null);
        tabdangki.setIndicator(res.getString(R.string.tabdangki), null);

        tabHost.addTab(tabdangnhap, DangNhapFragment.class, null);
        tabHost.addTab(tabdangki,DangKiFragment.class, null);
    }

}
