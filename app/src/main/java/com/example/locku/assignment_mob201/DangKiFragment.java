package com.example.locku.assignment_mob201;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DangKiFragment extends Fragment implements View.OnClickListener{
    private Button btnRegister;
    private EditText etEmail, etPass;
    private DbHelper db;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tabdangki, container, false);
        db = new DbHelper(getActivity());
        btnRegister = (Button)view.findViewById(R.id.btn_register);
        etEmail = (EditText)view.findViewById(R.id.ett_email);
        etPass = (EditText)view.findViewById(R.id.ett_password);
        btnRegister.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_register:
                register();
                break;
        }
    }

    // Phương thức đăng kí
    private void register(){
        // Nếu email không đúng hoặc bỏ trống thì sẽ báo lỗi là " Ivinalid Email "
       if (!validateEmail(etEmail.getText().toString())){
           etEmail.setError("Invalid Email");
           etEmail.requestFocus();
       }
       // Nếu password nhập lớn hơn 9 kí tự hoặc bỏ trống thí sẽ báo lỗi là " Invalid Password "
       else if (validatePassword(etPass.getText().toString())){
           etEmail.setError("Invalid Password");
           etEmail.requestFocus();
       }
        // Nếu đăng kí thành công thì sẽ được báo bằng " Đăng kí thành công "
       else {
                db.addUser(etEmail.getText().toString(), etPass.getText().toString());
           AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
           builder.setTitle("MONEY PIGS");
           builder.setMessage("Bạn đăng kí thành công rồi đấy ^_^");
           AlertDialog alertDialog = builder.create();
           alertDialog.show();
            }
        }

    // Báo lỗi password khi nhập lớn hơn kí tự là 9
    protected boolean validatePassword(String password) {
        if(password.isEmpty() && password.length()>9) {
            return true;
        } else {
            return false;
        }
    }

    // Báo lỗi email khi không nhập đúng cú pháp email
    protected boolean validateEmail(String email) {
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

}