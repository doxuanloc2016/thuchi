package com.example.locku.assignment_mob201;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.SyncStateContract;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;


public class DbHelper extends SQLiteOpenHelper {
    public static final String TAG = DbHelper.class.getSimpleName();
    public static final String DB_NAME = "myapp.db";
    public static final int DB_VERSION = 1;

    public static final String USER_TABLE = "users";
    public static final String MONEY_TABLE = "moneys";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PASS = "password";
    public static final String COLUMN_IDMONEY = "_idmoney";
    public static final String COLUMN_MONEY = "money";
    public static final String COLUMN_GHICHU = "ghichu";
    public static final String COLUMN_NGAYTHANG = "ngaythang";

    public static final String CREATE_TABLE_USERS = "CREATE TABLE " + USER_TABLE + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY,"
            + COLUMN_EMAIL + " TEXT NOT NULL,"
            + COLUMN_PASS + " TEXT NOT NULL);";

    public static final String CREATE_TABLE_MONEYS = "CREATE TABLE " + MONEY_TABLE + "("
            + COLUMN_IDMONEY + " INTEGER PRIMARY KEY,"
            + COLUMN_MONEY + " TEXT NOT NULL,"
            + COLUMN_GHICHU + " TEXT NOT NULL,"
            + COLUMN_NGAYTHANG + " DATETIME);";

    public DbHelper(FragmentActivity context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USERS);
        db.execSQL(CREATE_TABLE_MONEYS);
        db.execSQL("INSERT INTO " +USER_TABLE+ " VALUES(1,'admin@gmail.com','admin')");
    }

    // Hiển thị tiền, ghi chú, ngày tháng thêm vào
    public ArrayList<Money> getAllMoney(){
        ArrayList<Money> ds =new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor c=db.rawQuery("SELECT * FROM " + MONEY_TABLE, null);
        if(c.moveToFirst()){
            do{
                String idmoney = c.getString(0);
                int money = Integer.parseInt(c.getString(1));
                String ghichu = c.getString(2);
                String ngaythang = c.getString(3);
                Money l = new Money(idmoney, money, ghichu, ngaythang);
                ds.add(l);
            }while(c.moveToNext());
        }
        return ds;
    }

    // Thêm tiền
    public void ThemMoney(Money money){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("_idmoney", money.id);
        values.put("money", money.tien);
        values.put("ghichu", money.ghichu);
        values.put("ngaythang", money.NgayThang);
        db.insert(MONEY_TABLE, null, values);
        db.close();
    }

    // Thu
    public int thutien(){
        SQLiteDatabase db = this.getWritableDatabase();
        int result = 0;
        String select = " SELECT " + "SUM(MONEY)" + " AS RESULT " + "FROM MONEYS ";
        Cursor cursor = db.rawQuery(select, null);
        if (cursor.moveToFirst()){
            do{
                result = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        return result;
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE);
        onCreate(db);
    }

    // Lấy username để đăng nhập
    public boolean getUser(String email, String pass){
        String selectQuery = "SELECT * FROM  " + USER_TABLE + " WHERE " +
                COLUMN_EMAIL + " = " + "'"+email+"'" + " AND " + COLUMN_PASS + " = " + "'"+pass+"'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {

            return true;
        }
        cursor.close();
        db.close();

        return false;
    }

    // Đăng kí username
    public void addUser(String email, String password) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_EMAIL, email);
        values.put(COLUMN_PASS, password);


        long id = db.insert(USER_TABLE, null, values);
        db.close();

        Log.d(TAG, "user inserted" + id);
    }

    // Hiện thị tất cả username dã đăng kí
    public ArrayList<ThanhVien> getAllThanhVien(){
        ArrayList<ThanhVien> ds = new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor c=db.rawQuery("SELECT * FROM " + USER_TABLE, null);
        if(c.moveToFirst()){
            do{
                String id = c.getString(0);
                String email = c.getString(1);
                String pass = c.getString(2);
                ThanhVien tv = new ThanhVien(id,email,pass);
                ds.add(tv);
            }while(c.moveToNext());
        }
        return ds;
    }

}
