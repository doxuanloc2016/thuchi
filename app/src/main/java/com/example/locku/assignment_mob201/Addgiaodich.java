package com.example.locku.assignment_mob201;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Addgiaodich extends AppCompatActivity implements View.OnClickListener {
    EditText etNgay,etGhiChu,etTien;
    ArrayList<Money> arrMoney;
    DbHelper dbHelper;
    FloatingActionButton btThemTien;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_giaodich);
        arrMoney = new ArrayList<Money>();
        dbHelper = new DbHelper(Addgiaodich.this);
        etGhiChu = (EditText) findViewById(R.id.etGhichu);
        etTien = (EditText) findViewById(R.id.etTien);
        btThemTien = (FloatingActionButton) findViewById(R.id.flbtThemTien);
        btThemTien.setOnClickListener(this);
        etTien.setHintTextColor(getResources().getColor(R.color.primary_dark));

        // Click để mở dialog Chọn ngày
        etNgay = (EditText) findViewById(R.id.etNgay);
        etNgay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(Addgiaodich.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    // Click để mở dialog Chọn ngày
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();

        }
    };
    private void updateLabel() {

        String myFormat = "dd-MM-yyyy"; //In which you need put here

        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        etNgay.setText(sdf.format(myCalendar.getTime()));

    }


    // Phương thức thêm tiền
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.flbtThemTien:
                loithemTien();
                break;
        }
    }

    //  Phương thức kiểm tra khi thêm tiền
    public boolean loithemTien(){
            String id = null;
            String tien = etTien.getText().toString();
            String ghichu = etGhiChu.getText().toString();
            String NgayThang = etNgay.getText().toString();
            if(tien.isEmpty()){
                AlertDialog.Builder builder = new AlertDialog.Builder(Addgiaodich.this);
                builder.setTitle("MONEY PIGS");
                builder.setMessage("Bạn chưa nhập tiền kìa");
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
            else if(ghichu.isEmpty()){
                AlertDialog.Builder builder = new AlertDialog.Builder(Addgiaodich.this);
                builder.setTitle("MONEY PIGS");
                builder.setMessage("Bạn chưa viết ghi chú kìa");
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
            else if(NgayThang.isEmpty()){
                AlertDialog.Builder builder = new AlertDialog.Builder(Addgiaodich.this);
                builder.setTitle("MONEY PIGS");
                builder.setMessage("Bạn chưa chọn ngày kìa");
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
            else {
                Integer money = Integer.valueOf(tien);
                Money m = new Money(id,money,ghichu,NgayThang);
                dbHelper.ThemMoney(m);
                Intent intent = new Intent(Addgiaodich.this,MainActivity.class);
                startActivity(intent);
                finish();
                return false;
            }
        }
}
