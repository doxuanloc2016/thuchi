package com.example.locku.assignment_mob201;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

public class ThanhVienAdapter extends BaseAdapter{
    Context context;
    ArrayList<ThanhVien> ds;
    ThanhVienAdapter(Context context, ArrayList<ThanhVien> ds){
        this.context = context;
        this.ds = ds;
    }

    public int getCount(){
        return ds.size();
    }

    @Override
    public Objects getItem(int position){
        return  null;
    }

    @Override
    public long getItemId(int position){
        return  0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view;
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        view = inflater.inflate(R.layout.activity_thanh_vien_adapter, null);
        TextView textView1 = (TextView) view.findViewById(R.id.textView1);
        TextView textView2 = (TextView) view.findViewById(R.id.textView2);
        ThanhVien tv = ds.get(position);
        textView1.setText(tv.id+"");
        textView2.setText(tv.email+"");
        return  view;
    }
}